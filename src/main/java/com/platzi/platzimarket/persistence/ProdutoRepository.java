package com.platzi.platzimarket.persistence;

import com.platzi.platzimarket.persistence.crud.ProductoCrudRepository;
import com.platzi.platzimarket.persistence.entity.Producto;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProdutoRepository {

    private ProductoCrudRepository productoCrudRepository;

    public List<Producto> getAll(){
        return (List<Producto>) productoCrudRepository.findAll();
    }

    public List<Producto> getByCategoria(int idCategoria){
        return productoCrudRepository.findByIdCategoriaOrderByNombreAsc(idCategoria);
    }

    public Optional<List<Producto>> getEscasos(int cantidad){
        return productoCrudRepository.findByCantidadStockLessThanAndEstado(cantidad,true);
    }

    public Optional<List<Producto>> getPrecioLessThan(double precio){
        return productoCrudRepository.findByPrecioVentaLessThan(precio);
    }

    public Optional<List<Producto>>  getNombresStartingWith(String letras){
        return productoCrudRepository.findByNombreStartingWith(letras);
    }

    public Optional<List<Producto>> getPreciosProductoBetween(double startingPrice, double finalPrice){
        return productoCrudRepository.findByPrecioVentaBetween(startingPrice,finalPrice);
    }

    public Optional<Producto> getProducto(int idProducto){
        return productoCrudRepository.findById(idProducto);
    }

   public Producto save(Producto producto){
        return productoCrudRepository.save(producto);
   }

    public void delete(int idProducto){
        productoCrudRepository.deleteById(idProducto);
    }

}
