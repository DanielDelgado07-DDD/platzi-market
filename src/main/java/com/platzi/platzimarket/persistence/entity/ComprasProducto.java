package com.platzi.platzimarket.persistence.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.EnableMBeanExport;

@Entity
@Table(name = "compras_productos")
@Getter
@Setter
@NoArgsConstructor
public class ComprasProducto {

    @EmbeddedId
    private ComprasProductoPK id;

    private Integer cantidad;

    private Double total;

    private Boolean estado;

    @ManyToOne
    @JoinColumn(name = "id_producto",insertable = false,updatable = false)
    private Producto producto;

    @ManyToOne
    @JoinColumn(name ="id_compra",insertable = false,updatable = false)
    private Compra compra;
}
