package com.platzi.platzimarket.persistence.crud;

import com.platzi.platzimarket.persistence.entity.Producto;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductoCrudRepository extends CrudRepository<Producto,Integer> {
//lista de productos que pertenezca a una categoria

    //Query nativo
    //@Query(value = "SELECT * FROM porductos WHERE id_categoria =? ",nativeQuery = true)
    //Query methods
    List<Producto> findByIdCategoriaOrderByNombreAsc(int idCategoria);
    //optionales  de las nuevas versiones de Java

    Optional<List<Producto>> findByCantidadStockLessThanAndEstado(int cantidadStock, boolean estado);

    Optional<List<Producto>> findByPrecioVentaLessThan(double precio);

    Optional <List<Producto>> findByNombreStartingWith(String letra);

    Optional <List<Producto>> findByPrecioVentaBetween(double startingPrice, double finalPrice );
}
