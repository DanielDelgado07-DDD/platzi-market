package com.platzi.platzimarket.domain.repository;

import com.platzi.platzimarket.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    List<Product> getAll();
    List<Product> getByCategory(int categoryId);
    Optional<List<Product>> getScareProducts(int quantity);
    Optional<List<Product>> getPriceLessThan(double price);
    Optional<List<Product>> getProductsStartingWith(String letters);
    Optional<List<Product>> getPriceProductBetween(double startingPrice,double finalPrice);
    Optional<Product> getProduct(int productId);
    Optional<Product> save(Product product);
    void delete(int productId);
}
