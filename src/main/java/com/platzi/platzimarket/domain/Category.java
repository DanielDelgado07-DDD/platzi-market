package com.platzi.platzimarket.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Category {
    private int categoryId;
    private String category;
    private boolean active;
}
